import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DefaultComponent } from './default/default.component';

const appRoutes: Routes = [
{path: '',  redirectTo: 'login', pathMatch: 'full'},
{path:'home', component: DefaultComponent},
{path:'login', component: LoginComponent},
{path:'login/:id', component: LoginComponent},
{path:'register', component: RegisterComponent},
{path:'**', component: LoginComponent},
];

export const  appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);