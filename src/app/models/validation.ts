export class Validations {
	constructor(
		public isRequired: string,
		public isValid: string,
		public isEqual: string) {}
}