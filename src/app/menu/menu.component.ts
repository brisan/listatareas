import { Component, OnInit, Input } from '@angular/core';
import { UserService } from "../services/user.service";

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css'],
	providers: [UserService]
})
export class MenuComponent implements OnInit {

	@Input()
	public identity;

	@Input()
	public token;

	constructor(
		// private _userService: UserService
		){
		//this.identity = this._userService.getIdentity();
		//this.token = this._userService.getToken();
	}

	ngOnInit(){
		console.log({identity_localStorage: this.identity});
		console.log({token_localStorage: this.token});
	}

}
