import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class UserService {
	public url: string;
	public identity;
	public token;

	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	singup(params){
		let jsonParams = JSON.stringify(params);
		// let header = new Headers({"Content-Type": "aplication/x-www-form-urlencoded"});
		// let headers = new Headers({ 'Content-Type': 'application/json' });
		// let options = new RequestOptions({ headers: headers });
		
		let respose = this._http.post(this.url, jsonParams).map(res => res.json());
		console.log({servicio: respose});
		return respose;
	}

	getIdentity(){
		let identity = JSON.parse(localStorage.getItem('identity'));

		if(typeof identity != 'undefined'){
			this.identity = identity;
		}else{
			this.identity = null;
		}

		return this.identity;
	}

	getToken(){
		let token = JSON.parse(localStorage.getItem('token'));

		if(typeof token != 'undefined'){
			this.token = token;
		}else{
			this.token = null;
		}

		return this.token;
	}
}