import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { UserService } from "../services/user.service";

@Component({
	selector: 'app-default',
	templateUrl: './default.component.html',
	styleUrls: ['./default.component.css'],
	providers: [UserService]
})
export class DefaultComponent implements OnInit {

	public identity;
	public token;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService
		){
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();

		if(this.identity == null){
			this._router.navigate(['login']);
		}
	}

	ngOnInit(){
		console.log(this.identity);
	}

}
