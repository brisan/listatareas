import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { User} from "../models/user";
import { Validations } from "../models/validation";

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

	public title: string;
	public user: User;
	public validations: Validations;
	public pattern;

	constructor(){
		this.title = "Registrar Usuario";

		this.user = new User(1, "", "user", "", "", "");

		this.validations = new Validations(
			"El campo es requerido",
			"El valor no es valido",
			"El valor no coincide"
			);

		this.pattern = {
			isMail: /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/
		};
	}

	ngOnInit(){
		console.log({"RegisterComponent": "El componente ha sido cargado"});
	}

}
