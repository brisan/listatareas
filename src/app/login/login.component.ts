import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { UserService } from "../services/user.service" 

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	providers: [UserService],
	inputs: ['data'],
})
export class LoginComponent implements OnInit {
	public title: string;
	public user;
	public sendData;
	public identity;
	public token;
	public error;

	@Input() data;
	
	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService
		){
		this.title = "Login";
		this.user = {
			email: "",
			password: "",
		};

		this.sendData = {
			email: "",
			password: "",
			gethash: false
		};

		this.error = false;

		

	}
	
	ngOnInit(){
		console.log({"LoginComponent": this.data});
		this.logout();
	}

	logout(){
		this._route.params.forEach((params: Params) => {
			console.log({parametros_ruter: params});
			let logout = parseInt(params['id']);
			if(logout == 1){
				localStorage.removeItem('identity');
				localStorage.removeItem('token');
				this.identity = null;
				this.token = null;
				window.location.href = '/login';
				//this._router.navigate(['login']);
			}
		});
	}

	onSubmit(params){
		if(params === true){
			this.sendData.email = btoa(this.user.email);
			this.sendData.password = btoa(this.user.password);
			this.sendData.gethash = false;
			this._userService.singup(this.sendData).subscribe(
				respose => {
					if(respose.status === true){
						this.error = false;
						this.identity = respose.identity;
						localStorage.setItem('identity', JSON.stringify(this.identity));

						this.sendData.gethash = true;
						this._userService.singup(this.sendData).subscribe(
							respose => {
								if(respose.status === true){
									this.error = false;
									this.token = respose.token;
									// this.GetToken.emit({token: this.token});
									localStorage.setItem('token', JSON.stringify(this.token));
									//this._router.navigate(['home']);
									window.location.href = '/home';
								}
							},
							error => {
								this.error = true;
								this.token = null;
								console.log({respuesta: error});
							}
							);

					}
				},
				error => {
					this.error = true;
					this.identity = null;
					console.log({respuesta: error});
				}
				);
		}
	}
}